public class SquareRoot {
    public int Sqrt( int[] arr,int x) {
        if (x == 0) {
            return 0;
        }
        else {
            int left = 1;
            int right = arr.length - 1;

            while (left <= right) {
                int mid = left + (right - left) / 2;
                if (arr[mid] > x / mid) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }

            return left - 1;
        }
    }
    public void printInputOutput(int[] arr,int x){
        System.out.println("Input: x = "+x);
        System.out.println("Output: "+Sqrt(arr,x));
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        SquareRoot solution = new SquareRoot();
        solution.printInputOutput(arr,2);
        solution.printInputOutput(arr,25);
    }
}